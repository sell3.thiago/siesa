<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Adoption extends Model
{
    use HasFactory;

    use SoftDeletes;
    public $table = 'adoptions';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'pet_id', 
        'owner_id',
        'comments'
    ];
}
