<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends Model
{
    use HasFactory;
    use SoftDeletes;
    public $table = 'owners';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'owner_name',
        'address',
        'phone',
        'email',
        'comments'
    ];
}
