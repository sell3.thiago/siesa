<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pet extends Model
{
    use HasFactory;
    use SoftDeletes;
    public $table = 'pets';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name_pet',
        'pet_type_id'
    ];
}
