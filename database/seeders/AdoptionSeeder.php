<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Adoption;

class AdoptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Adoption::create([
            'pet_id' => 1,
            'owner_id' => 1,
            'comments' => 'Adoptado'
        ]);

        Adoption::create([
            'pet_id' => 2,
            'owner_id' => 1,
            'comments' => 'Adoptado'
        ]);

        Adoption::create([
            'pet_id' => 3,
            'owner_id' => 2,
            'comments' => 'Adoptado'
        ]);

        Adoption::create([
            'pet_id' => 4,
            'owner_id' => 3,
            'comments' => 'Adoptado'
        ]);

        Adoption::create([
            'pet_id' => 5,
            'owner_id' => 4,
            'comments' => 'Adoptado'
        ]);

        Adoption::create([
            'pet_id' => 6,
            'owner_id' => 4,
            'comments' => 'Adoptado'
        ]);

        Adoption::create([
            'pet_id' => 7,
            'owner_id' => 6,
            'comments' => 'Adoptado'
        ]);

        Adoption::create([
            'pet_id' => 8,
            'owner_id' => 7,
            'comments' => 'Adoptado'
        ]);
    }
}
