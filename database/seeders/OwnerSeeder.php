<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Owner;

class OwnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Owner::create([
            'owner_name' => 'Thiago Lopez',
            'address' => 'Monteria',
            'phone' => '3125648751',
            'email' => 'thiago@email.com',
            'comments' => 'Ninguno'
        ]);

        Owner::create([
            'owner_name' => 'Maria Aguilar',
            'address' => 'Tierralta, Córdoba',
            'phone' => '3214587412',
            'email' => 'mariagui@email.com',
            'comments' => 'Ninguno'
        ]);

        Owner::create([
            'owner_name' => 'Carolina Martinez',
            'address' => 'Sincelejo',
            'phone' => '3001214547',
            'email' => 'carohMar@email.com',
            'comments' => 'Veterinaria'
        ]);

        Owner::create([
            'owner_name' => 'Carlos San Martin',
            'address' => 'Cartagena',
            'phone' => '3012451200',
            'email' => 'carlitosanmart@email.com',
            'comments' => 'Refugio de animales'
        ]);

        Owner::create([
            'owner_name' => 'Maria Martinez A.',
            'address' => 'Montelibano, Córdoba',
            'phone' => '3201458740',
            'email' => 'mariam@email.com',
            'comments' => 'Ninguno'
        ]);

        Owner::create([
            'owner_name' => 'Daniel Mendoza',
            'address' => 'Bogotá',
            'phone' => '3210541078',
            'email' => 'daniel@email.com',
            'comments' => 'Tienda de mascotas'
        ]);

        Owner::create([
            'owner_name' => 'Carmen Diaz',
            'address' => 'Sincelejo',
            'phone' => '3201589654',
            'email' => 'carmendiaz@email.com',
            'comments' => 'Tienda de mascotas'
        ]);

        Owner::create([
            'owner_name' => 'Ricardo Espinoza',
            'address' => 'Montería',
            'phone' => '3000120145',
            'email' => 'ricardo@email.com',
            'comments' => 'Veterinario'
        ]);

        Owner::create([
            'owner_name' => 'Martin Bernal',
            'address' => 'Monteria',
            'phone' => '3145824170',
            'email' => 'martbern@email.com',
            'comments' => 'Ninguno'
        ]);

        Owner::create([
            'owner_name' => 'Camilo Viera',
            'address' => 'Pto Libertador, Córdoba',
            'phone' => '3236548521',
            'email' => 'viera@email.com',
            'comments' => 'Tienda de mascotas'
        ]);
    }
}
