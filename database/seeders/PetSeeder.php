<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pet;

class PetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pet::create([
            'name_pet' => 'DRAKO',
            'pet_type_id' => 1
        ]);

        Pet::create([
            'name_pet' => 'JEYKON',
            'pet_type_id' => 1
        ]);

        Pet::create([
            'name_pet' => 'REINALS',
            'pet_type_id' => 2
        ]);

        Pet::create([
            'name_pet' => 'LUPE',
            'pet_type_id' => 1
        ]);

        Pet::create([
            'name_pet' => 'BIGOTES',
            'pet_type_id' => 4
        ]);

        Pet::create([
            'name_pet' => 'OBITO',
            'pet_type_id' => 1
        ]);
        
        Pet::create([
            'name_pet' => 'NEMO',
            'pet_type_id' => 5
        ]);

        Pet::create([
            'name_pet' => 'GUIDEON',
            'pet_type_id' => 3
        ]);

        Pet::create([
            'name_pet' => 'PIOLIN',
            'pet_type_id' => 3
        ]);

        Pet::create([
            'name_pet' => 'ROY',
            'pet_type_id' => 1
        ]);

        Pet::create([
            'name_pet' => 'PEPE',
            'pet_type_id' => 2
        ]);

        Pet::create([
            'name_pet' => 'AGGI',
            'pet_type_id' => 1
        ]);

        Pet::create([
            'name_pet' => 'ODIE',
            'pet_type_id' => 2
        ]);

        Pet::create([
            'name_pet' => 'BLANCO',
            'pet_type_id' => 4
        ]);

        Pet::create([
            'name_pet' => 'FLASH',
            'pet_type_id' => 2
        ]);

        Pet::create([
            'name_pet' => 'TEO',
            'pet_type_id' => 2
        ]);
    }
}
