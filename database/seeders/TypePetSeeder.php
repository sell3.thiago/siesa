<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PetType;
class TypePetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PetType::create([
            'title' => 'Perros',
            'description' => 'N/A'
        ]);
        PetType::create([
            'title' => 'Aves',
            'description' => 'N/A'
        ]);
        PetType::create([
            'title' => 'Gatos',
            'description' => 'N/A'
        ]);
        PetType::create([
            'title' => 'Hámsters',
            'description' => 'N/A'
        ]);
        PetType::create([
            'title' => 'Peces',
            'description' => 'N/A'
        ]);       
    }
}
